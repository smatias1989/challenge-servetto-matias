package com.matiasservetto.coorvacontacts.list.get;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.matiasservetto.coorvacontacts.models.Contact;

import java.lang.ref.WeakReference;
import java.util.List;

public class LoadContactsUseCase {

    private BackgroundJob job;

    private LoadContactsUseCase() {

    }

    @NonNull
    public static LoadContactsUseCase run(@NonNull Listener listener) {
        LoadContactsUseCase instance = new LoadContactsUseCase();
        instance.job = new BackgroundJob(listener);
        instance.job.execute();
        return instance;
    }

    private static class BackgroundJob extends AsyncTask<Void, Void, List<Contact>> {
        @NonNull
        private final WeakReference<Listener> listener;
        @NonNull
        private final LoadContactsService service;

        private BackgroundJob(@NonNull Listener listener) {
            this.listener = new WeakReference<>(listener);
            service = new LoadContactsService();
        }

        @Override
        protected List<Contact> doInBackground(Void... voids) {
            return service.run();
        }

        @Override
        protected void onPostExecute(List<Contact> contacts) {
            if (listener.get() != null) {
                if (contacts != null) {
                    listener.get().onContactLoaded(contacts);
                } else {
                    listener.get().onError("Error loading contacts");
                }
            }
        }
    }

    public interface Listener {
        void onContactLoaded(@NonNull List<Contact> contacts);
        void onError(@NonNull String message);
    }
}

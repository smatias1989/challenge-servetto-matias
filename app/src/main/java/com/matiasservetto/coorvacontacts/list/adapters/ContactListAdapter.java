package com.matiasservetto.coorvacontacts.list.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.matiasservetto.coorvacontacts.R;
import com.matiasservetto.coorvacontacts.models.Contact;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListItemViewHolder> {

    @NonNull
    private final WeakReference<Listener> listener;
    @NonNull
    private final List<Contact> contacts;

    public ContactListAdapter(@NonNull Listener listener) {
        this.listener = new WeakReference<>(listener);
        this.contacts = new ArrayList<>();
    }

    public void setContacts(@NonNull List<Contact> contacts) {
        this.contacts.clear();
        this.contacts.addAll(contacts);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ContactListItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_list_item, viewGroup, false);
        return new ContactListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactListItemViewHolder viewHolder, int i) {
        Contact contact = contacts.get(i);
        viewHolder.bind(contact, listener);
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public interface Listener {
        void onContactSelected(@NonNull Contact contact);
    }
}

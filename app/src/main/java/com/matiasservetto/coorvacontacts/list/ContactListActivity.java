package com.matiasservetto.coorvacontacts.list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.matiasservetto.coorvacontacts.R;
import com.matiasservetto.coorvacontacts.contacts.form.ContactActivity;
import com.matiasservetto.coorvacontacts.list.adapters.ContactListAdapter;
import com.matiasservetto.coorvacontacts.models.Contact;

import java.util.ArrayList;
import java.util.List;

import static com.matiasservetto.coorvacontacts.contacts.form.ContactActivity.EXTRA_DELETED_CONTACT_RESULT;
import static com.matiasservetto.coorvacontacts.contacts.form.ContactActivity.EXTRA_EDITED_CONTACT;
import static com.matiasservetto.coorvacontacts.contacts.form.ContactActivity.EXTRA_EDITED_CONTACT_RESULT;

public class ContactListActivity extends AppCompatActivity implements ContactListPresenter.View, ContactListAdapter.Listener {

    private static final int REQUEST_NEW_CONTACT = 435;
    private static final int REQUEST_VIEW_CONTACT = 123;
    private static final String OUT_STATE_CONTACTS = "OUT_STATE_CONTACTS";

    @Nullable
    private ViewHolder views;
    @Nullable
    private ContactListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        views = new ViewHolder(this, this);
        bindListeners(views);

        presenter = new ContactListPresenter(this);
        if (savedInstanceState != null) {
            presenter.contacts = savedInstanceState.getParcelableArrayList(OUT_STATE_CONTACTS);
        }
        presenter.loadContacts();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_NEW_CONTACT && resultCode == RESULT_OK) {
            refreshContactList();
        } if (requestCode == REQUEST_VIEW_CONTACT && resultCode == EXTRA_EDITED_CONTACT_RESULT) {
            if (data != null) {
                Contact contact = data.getParcelableExtra(EXTRA_EDITED_CONTACT);
                if (presenter != null) {
                    presenter.onEditedContact(contact);
                }
            } else {
                refreshContactList();
            }
        } else if (requestCode == REQUEST_VIEW_CONTACT && resultCode == EXTRA_DELETED_CONTACT_RESULT) {
            if (data != null) {
                Contact contact = data.getParcelableExtra(EXTRA_EDITED_CONTACT);
                if (presenter != null) {
                    presenter.onDeletedContact(contact);
                }
            } else {
                refreshContactList();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null && presenter.contacts != null) {
            outState.putParcelableArrayList(OUT_STATE_CONTACTS, new ArrayList<Parcelable>(presenter.contacts));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.views = null;
        this.presenter = null;
    }

    private void bindListeners(@NonNull ViewHolder views) {
        views.newContactAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNewContactActivity();
            }
        });
        views.loading.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContactList();
            }
        });
    }

    private void refreshContactList() {
        if (presenter != null) {
            presenter.refreshContacts();
        }
    }

    private void startNewContactActivity() {
        startActivityForResult(ContactActivity.newIntent(this, null), REQUEST_NEW_CONTACT);
    }

    @Override
    public void showLoadingListViews() {
        if (views != null) {
            views.loading.setRefreshing(true);
        }
    }

    @Override
    public void showContactListViews(@NonNull List<Contact> contacts) {
        if (views != null) {
            views.adapter.setContacts(contacts);
            views.loading.setRefreshing(false);
        }
    }

    @Override
    public void showErrorMessageViews(@NonNull String message) {
        if (views != null) {
            Snackbar.make(views.list, message, Snackbar.LENGTH_LONG).show();
            views.loading.setRefreshing(false);
        }
    }

    @Override
    public void onContactSelected(@NonNull Contact contact) {
        startActivityForResult(ContactActivity.newIntent(this, contact), REQUEST_VIEW_CONTACT);
    }

    private static class ViewHolder {

        final FloatingActionButton newContactAction;
        final SwipeRefreshLayout loading;
        final RecyclerView list;
        final ContactListAdapter adapter;

        ViewHolder(@NonNull Activity activity, @NonNull ContactListAdapter.Listener listener) {
            newContactAction = activity.findViewById(R.id.contact_list_new_contact_action);
            loading = activity.findViewById(R.id.contact_list_loading);
            list = activity.findViewById(R.id.contact_list);
            list.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
            adapter = new ContactListAdapter(listener);
            list.setAdapter(adapter);
        }
    }
}

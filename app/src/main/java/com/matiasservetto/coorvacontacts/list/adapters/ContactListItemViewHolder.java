package com.matiasservetto.coorvacontacts.list.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.matiasservetto.coorvacontacts.R;
import com.matiasservetto.coorvacontacts.models.Contact;

import java.lang.ref.WeakReference;

public class ContactListItemViewHolder  extends RecyclerView.ViewHolder {

    @NonNull
    private final ImageView avatar;
    @NonNull
    private final View actionable;
    @NonNull
    private final TextView name;

    public ContactListItemViewHolder(@NonNull View itemView) {
        super(itemView);
        avatar = itemView.findViewById(R.id.contact_list_item_avatar);
        name = itemView.findViewById(R.id.contact_list_item_name);
        actionable = itemView.findViewById(R.id.contact_list_item_actionable);
    }

    public void bind(@NonNull final Contact contact, @NonNull final WeakReference<ContactListAdapter.Listener> listener) {
        String name = String.format("%s %s", contact.firstName, contact.lastName);
        this.name.setText(name);
        if (contact.photo != null) {
            this.avatar.setImageBitmap(contact.photo);
        } else {
            this.avatar.setImageResource(R.drawable.ic_account_circle_gray_24dp);
        }
        this.actionable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener.get() != null) {
                    listener.get().onContactSelected(contact);
                }
            }
        });
    }
}

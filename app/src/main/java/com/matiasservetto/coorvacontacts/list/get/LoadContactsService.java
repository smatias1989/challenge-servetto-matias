package com.matiasservetto.coorvacontacts.list.get;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.matiasservetto.coorvacontacts.models.Contact;
import com.matiasservetto.coorvacontacts.networking.BaseService;
import com.matiasservetto.coorvacontacts.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

class LoadContactsService extends BaseService<LoadContactsService.ApiService> {

    public interface ApiService {
        @GET("contacts/")
        Call<List<ApiContact>> getList();
    }

    @NonNull
    private final ApiService service;

    public LoadContactsService() {
        service = createService(ApiService.class);
    }

    @Nullable
    public List<Contact> run() {
        List<ApiContact> response;
        try {
            response = service.getList().execute().body();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return convertToValidModel(response);
    }

    @Nullable
    private List<Contact> convertToValidModel(@Nullable List<ApiContact> response) {
        List<Contact> contacts = new ArrayList<>();
        if (response != null) {
            for (ApiContact contact : response) {
                createValidContact(contacts, contact);
            }
        }
        if (contacts.size() == 0) {
            return null;
        }
        Collections.sort(contacts);
        return contacts;
    }

    private void createValidContact(@NonNull List<Contact> contacts, @NonNull ApiContact contact) {
        if (StringUtils.isNotEmpty(contact.id)
                && StringUtils.isNotEmpty(contact.firstName)
                && StringUtils.isNotEmpty(contact.lastName)
                && StringUtils.isNotEmpty(contact.email)) {
            contacts.add(new Contact(contact.id, contact.firstName, contact.lastName, contact.email, contact.phone, contact.photo));
        }
    }

    public class ApiContact {

        @Nullable
        String id;
        @SerializedName("first_name")
        @Nullable
        String firstName;
        @SerializedName("last_name")
        @Nullable
        String lastName;
        @Nullable
        String email;
        @Nullable
        String phone;
        @Nullable
        Bitmap photo;
    }
}

package com.matiasservetto.coorvacontacts.list;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.matiasservetto.coorvacontacts.list.get.LoadContactsUseCase;
import com.matiasservetto.coorvacontacts.models.Contact;
import com.matiasservetto.coorvacontacts.utils.StringUtils;

import java.lang.ref.WeakReference;
import java.util.List;

class ContactListPresenter implements LoadContactsUseCase.Listener {

    @NonNull
    private final WeakReference<View> view;
    @Nullable
    public List<Contact> contacts;

    public ContactListPresenter(@NonNull View view) {
        this.view = new WeakReference<>(view);
    }

    public void loadContacts() {
        if (view.get() != null) {
            if (contacts == null) {
                view.get().showLoadingListViews();
                LoadContactsUseCase.run(this);
            } else {
                onContactLoaded(contacts);
            }
        }
    }

    public void refreshContacts() {
        if (view.get() != null) {
            view.get().showLoadingListViews();
            LoadContactsUseCase.run(this);
        }
    }

    @Override
    public void onContactLoaded(@NonNull List<Contact> contacts) {
        if (view.get() != null) {
            this.contacts = contacts;
            view.get().showContactListViews(contacts);
        }
    }

    @Override
    public void onError(@NonNull String message) {
        if (view.get() != null) {
            view.get().showErrorMessageViews(message);
        }
    }

    public void onEditedContact(@Nullable Contact contact) {
        if (view.get() != null && contacts != null
                && contact != null && StringUtils.isNotEmpty(contact.id)) {
            Contact contactById = findContactById(contacts, contact);
            if (contactById != null) {
                contactById.update(contact);
                view.get().showContactListViews(contacts);
            }
        }
    }

    public void onDeletedContact(@Nullable Contact contact) {
        if (view.get() != null && contacts != null
                && contact != null && StringUtils.isNotEmpty(contact.id)) {
            Contact contactById = findContactById(contacts, contact);
            if (contactById != null) {
                contacts.remove(contactById);
                view.get().showContactListViews(contacts);
            }
        }
    }

    @Nullable
    private Contact findContactById(@NonNull List<Contact> contacts, @NonNull Contact contact) {
        for (Contact it : contacts) {
            if (StringUtils.isNotEmpty(it.id) && it.id.equalsIgnoreCase(contact.id)) {
                return it;
            }
        }
        return null;
    }

    public interface View {

        void showLoadingListViews();

        void showContactListViews(@NonNull List<Contact> contacts);

        void showErrorMessageViews(@NonNull String message);
    }
}

package com.matiasservetto.coorvacontacts.contacts.form;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.matiasservetto.coorvacontacts.contacts.form.creation.NewContactUseCase;
import com.matiasservetto.coorvacontacts.contacts.form.delete.DeleteContactUseCase;
import com.matiasservetto.coorvacontacts.contacts.form.edition.EditContactError;
import com.matiasservetto.coorvacontacts.contacts.form.edition.EditContactUseCase;
import com.matiasservetto.coorvacontacts.models.Contact;
import com.matiasservetto.coorvacontacts.utils.StringUtils;

import java.lang.ref.WeakReference;

public class ContactPresenter implements NewContactUseCase.Listener, EditContactUseCase.Listener, DeleteContactUseCase.Listener {

    private static final int SIZE = 64;

    @NonNull
    private final WeakReference<View> view;
    @Nullable
    public final Contact editionContact;

    public ContactPresenter(@NonNull View view, @Nullable Contact editionContact) {
        this.view = new WeakReference<>(view);
        this.editionContact = editionContact;
    }

    public void createNewContact(@NonNull String firstName, @NonNull String lastName, @NonNull String email, @NonNull String phone) {
        if (view.get() != null) {
            view.get().showCreatingNewUserViews();
            NewContactUseCase.run(new Contact(null, firstName, lastName, email, phone, null), this);
        }
    }

    public void editContact(@NonNull String firstName, @NonNull String lastName, @NonNull String email, @NonNull String phone) {
        if (view.get() != null && editionContact != null) {
            editionContact.firstName = firstName;
            editionContact.lastName = lastName;
            editionContact.email = email;
            editionContact.phone = phone;
            view.get().showEditingContactViews();
            EditContactUseCase.run(editionContact, this);
        }
    }

    public void loadForm() {
        View view = this.view.get();
        if (view != null && editionContact != null) {
            view.showContactViews(editionContact);
        }
        updateFormActionViews();
    }

    public void updateFormActionViews() {
        View view = this.view.get();
        if (view != null) {
            if (editionContact != null) {
                if (StringUtils.isNotEmpty(editionContact.id)) {
                    view.showEditContactActionViews();
                } else {
                    view.hideActionViews();
                }
            } else {
                view.showNewContactActionViews();
            }
        }
    }

    @Override
    public void onNewContactCreated() {
        if (view.get() != null) {
            view.get().showNewContactCreatedViews();
        }
    }

    @Override
    public void onContactEdited() {
        if (view.get() != null) {
            view.get().showEditedContactViews();
        }
    }

    @Override
    public void onContactDeleted() {
        if (view.get() != null) {
            view.get().showContactDeletedViews();
        }
    }

    @Override
    public void onEditError(@NonNull EditContactError error) {
        if (view.get() != null) {
            view.get().showErrorViews(error);
        }
    }

    public void deleteContact() {
        if (view.get() != null && editionContact != null) {
            view.get().showEditingContactViews();
            DeleteContactUseCase.run(editionContact, this);
        }
    }

    public void onImageCaptured(@NonNull Bitmap bitmap) {
        if (view.get() != null && editionContact != null) {
            // FIXME Reduce image in bg
            int maxSize = SIZE * SIZE;
            if (maxSize < bitmap.getWidth() * bitmap.getHeight()) {
                int width = SIZE, height = SIZE;
                if (bitmap.getHeight() > bitmap.getWidth()) {
                    height *= bitmap.getHeight() / (double) bitmap.getWidth();
                }
                if (bitmap.getHeight() < bitmap.getWidth()) {
                    width *= bitmap.getWidth() / (double) bitmap.getHeight();
                }
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
            }
            editionContact.photo = bitmap;
        }
    }

    public interface View {

        void showCreatingNewUserViews();

        void showNewContactCreatedViews();

        void showEditingContactViews();

        void showEditedContactViews();

        void showErrorViews(@NonNull EditContactError error);

        void showContactViews(@NonNull Contact contact);

        void showEditContactActionViews();

        void hideActionViews();

        void showNewContactActionViews();

        void showContactDeletedViews();
    }
}

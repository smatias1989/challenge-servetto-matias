package com.matiasservetto.coorvacontacts.contacts.form.delete;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.matiasservetto.coorvacontacts.contacts.form.edition.EditContactError;
import com.matiasservetto.coorvacontacts.contacts.form.networking.ContactServiceUtils;
import com.matiasservetto.coorvacontacts.models.Contact;
import com.matiasservetto.coorvacontacts.networking.BaseService;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class DeleteContactService extends BaseService<String> {

    public interface ApiService {
        @GET("contacts/remove/")
        Call<String> deleteContact(@Query("id") String id); // FIXME Response object came in string. should fix how to avois this problem and use Call<ApiResponse>
    }

    @NonNull
    private final ApiService service;

    public DeleteContactService() {
        service = createService(ApiService.class);
    }

    @Nullable
    public EditContactError run(@NonNull Contact contact) {
        String rawResponse = null;
        try {
            rawResponse = service.deleteContact(contact.id).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ContactServiceUtils.parseResponse(rawResponse,"Error deleting user");
    }
}

package com.matiasservetto.coorvacontacts.contacts.form.edition;

import android.support.annotation.NonNull;

public class EditContactError {

    @NonNull
    public final String message;

    public EditContactError(@NonNull String message) {
        this.message = message;
    }
}

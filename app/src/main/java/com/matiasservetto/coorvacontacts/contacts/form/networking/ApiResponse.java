package com.matiasservetto.coorvacontacts.contacts.form.networking;

import android.support.annotation.Nullable;

public class ApiResponse {
    @Nullable
    Integer status;
    @Nullable
    String message;
}
package com.matiasservetto.coorvacontacts.contacts.form;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.matiasservetto.coorvacontacts.R;
import com.matiasservetto.coorvacontacts.contacts.form.edition.EditContactError;
import com.matiasservetto.coorvacontacts.models.Contact;

public class ContactActivity extends AppCompatActivity implements ContactPresenter.View {

    private static final String EXTRA_CONTACT = "EXTRA_CONTACT";
    public static final String EXTRA_EDITED_CONTACT = "EXTRA_EDITED_CONTACT";
    public static final int EXTRA_EDITED_CONTACT_RESULT = 888;
    public static final int EXTRA_DELETED_CONTACT_RESULT = 999;

    private static final int REQUEST_IMAGE_CAPTURE = 446;
    private static final String OUT_STATE_CONTACT = "OUT_STATE_CONTACT";

    @Nullable
    private ViewHolder views;
    @Nullable
    private ContactPresenter presenter;
    private boolean edited = false;

    @NonNull
    public static Intent newIntent(@NonNull Context context, @Nullable Contact contact) {
        return new Intent(context, ContactActivity.class)
                .putExtra(EXTRA_CONTACT, contact);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact);
        setTitle(R.string.contact_new_title);
        views = new ViewHolder(this);
        bindListeners(views);
        Contact contact = (Contact) (savedInstanceState != null ? savedInstanceState.getParcelable(OUT_STATE_CONTACT) : getIntent().getParcelableExtra(EXTRA_CONTACT));
        setTitle(contact != null ? R.string.contact_title : R.string.contact_new_title);
        presenter = new ContactPresenter(this, contact);
        if (savedInstanceState == null) {
            presenter.loadForm();
            presenter.updateFormActionViews();
        } else {
            presenter.updateFormActionViews();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                if (presenter != null && imageBitmap != null) {
                    presenter.onImageCaptured(imageBitmap);
                    views.photo.setImageBitmap(imageBitmap);
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null && views != null && presenter.editionContact != null) {
            presenter.editionContact.firstName = views.firstName.getText().toString();
            presenter.editionContact.lastName = views.lastName.getText().toString();
            presenter.editionContact.email = views.email.getText().toString();
            presenter.editionContact.phone = views.phone.getText().toString();
            outState.putParcelable(OUT_STATE_CONTACT, presenter.editionContact);
        }
    }

    private void bindListeners(@NonNull ViewHolder views) {
        views.newContactAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewContact();
            }
        });
        views.editContactAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editContact();
            }
        });
        views.deleteContactAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteContact();
            }
        });
        views.photoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });
    }

    private void deleteContact() {
        if (presenter != null) {
            presenter.deleteContact();
        }
    }

    private void editContact() {
        if (presenter != null && views != null) {
            String firstName = views.firstName.getText().toString();
            String lastName = views.lastName.getText().toString();
            String email = views.email.getText().toString();
            String phone = views.phone.getText().toString();
            presenter.editContact(firstName, lastName, email, phone);
        }
    }

    private void createNewContact() {
        if (presenter != null && views != null) {
            String firstName = views.firstName.getText().toString();
            String lastName = views.lastName.getText().toString();
            String email = views.email.getText().toString();
            String phone = views.phone.getText().toString();
            presenter.createNewContact(firstName, lastName, email, phone);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        views = null;
        presenter = null;
    }

    @Override
    public void onBackPressed() {
        if (edited && presenter != null) {
            setResult(EXTRA_EDITED_CONTACT_RESULT, new Intent().putExtra(EXTRA_EDITED_CONTACT, presenter.editionContact));
            finishAfterTransition();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showCreatingNewUserViews() {
        showActionLoadingViewsVisible(true);
        setFormFieldsEditable(false);
    }

    private void showActionLoadingViewsVisible(boolean showLoading) {
        if (views != null) {
            views.newContactAction.setVisibility(showLoading ? View.INVISIBLE : View.VISIBLE);
            views.newLoading.setVisibility(showLoading ? View.VISIBLE : View.INVISIBLE);

            views.editContactAction.setVisibility(showLoading ? View.INVISIBLE : View.VISIBLE);
            views.deleteContactAction.setVisibility(showLoading ? View.INVISIBLE : View.VISIBLE);
            views.editLoading.setVisibility(showLoading ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private void setFormFieldsEditable(boolean editable) {
        if (views != null) {
            views.firstName.setEnabled(editable);
            views.lastName.setEnabled(editable);
            views.email.setEnabled(editable);
            views.phone.setEnabled(editable);
        }
    }

    @Override
    public void showNewContactCreatedViews() {
        if (views != null) {
            setResult(RESULT_OK);
            finishAfterTransition();
        }
    }

    @Override
    public void showEditingContactViews() {
        showActionLoadingViewsVisible(true);
        setFormFieldsEditable(false);
    }

    @Override
    public void showEditedContactViews() {
        showActionLoadingViewsVisible(false);
        setFormFieldsEditable(true);
        edited = true;
    }

    @Override
    public void showErrorViews(@NonNull EditContactError error) {
        if (views != null) {
            showActionLoadingViewsVisible(false);
            setFormFieldsEditable(true);
            Snackbar.make(findViewById(R.id.message_parent), error.message, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void showContactViews(@NonNull Contact contact) {
        if (views != null) {
            if (contact.photo != null) {
                views.photo.setImageBitmap(contact.photo);
            } else {
                views.photo.setImageResource(R.drawable.ic_account_circle_gray_24dp);
            }
            views.firstName.setText(contact.firstName);
            views.lastName.setText(contact.lastName);
            views.email.setText(contact.email);
            views.phone.setText(contact.phone);
        }
    }

    @Override
    public void showEditContactActionViews() {
        if (views != null) {
            showActionLoadingViewsVisible(false);
            setFormFieldsEditable(true);
            views.newActionContainer.setVisibility(View.GONE);
            views.editActionContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideActionViews() {
        if (views != null) {
            views.newActionContainer.setVisibility(View.GONE);
            views.editActionContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNewContactActionViews() {
        if (views != null) {
            showActionLoadingViewsVisible(false);
            setFormFieldsEditable(true);
            views.newActionContainer.setVisibility(View.VISIBLE);
            views.editActionContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContactDeletedViews() {
        Intent data = new Intent();
        if (presenter != null) {
            data.putExtra(EXTRA_EDITED_CONTACT, presenter.editionContact);
        }
        setResult(EXTRA_DELETED_CONTACT_RESULT, data);
        finishAfterTransition();

    }

    private static class ViewHolder {
        final View photoContainer;
        final ImageView photo;

        final EditText firstName;
        final EditText lastName;
        final EditText email;
        final EditText phone;

        final Button newContactAction;
        final View newLoading;
        final View newActionContainer;

        final Button editContactAction;
        final Button deleteContactAction;
        final View editLoading;
        final View editActionContainer;

        ViewHolder(Activity activity) {
            photoContainer = activity.findViewById(R.id.new_contact_image_container);
            photo = activity.findViewById(R.id.new_contact_image);

            firstName = activity.findViewById(R.id.new_contact_first_name);
            lastName = activity.findViewById(R.id.new_contact_last_name);
            email = activity.findViewById(R.id.new_contact_email);
            phone = activity.findViewById(R.id.new_contact_phone);

            newContactAction = activity.findViewById(R.id.new_contact_action);
            newLoading = activity.findViewById(R.id.new_contact_action_loading);
            newActionContainer = activity.findViewById(R.id.new_contact_action_container);

            editContactAction = activity.findViewById(R.id.edit_contact_action);
            deleteContactAction = activity.findViewById(R.id.delete_contact_action);
            editLoading = activity.findViewById(R.id.view_contact_action_loading);
            editActionContainer = activity.findViewById(R.id.view_contact_action_container);
        }
    }
}

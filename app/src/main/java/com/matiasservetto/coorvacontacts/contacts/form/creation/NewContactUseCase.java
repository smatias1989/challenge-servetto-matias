package com.matiasservetto.coorvacontacts.contacts.form.creation;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.matiasservetto.coorvacontacts.contacts.form.edition.EditContactError;
import com.matiasservetto.coorvacontacts.models.Contact;
import com.matiasservetto.coorvacontacts.utils.ContactValidator;

import java.lang.ref.WeakReference;

public class NewContactUseCase {

    private BackgroundJob job;

    private NewContactUseCase() {

    }

    @NonNull
    public static NewContactUseCase run(@NonNull Contact contact, @NonNull Listener listener) {
        NewContactUseCase instance = new NewContactUseCase();
        instance.job = new BackgroundJob(contact, listener);
        instance.job.execute();
        return instance;
    }

    private static class BackgroundJob extends AsyncTask<Void, Void, EditContactError> {
        @NonNull
        private final Contact contact;
        @NonNull
        private final WeakReference<Listener> listener;
        @NonNull
        private final NewContactService service;

        private BackgroundJob(@NonNull Contact contact, @NonNull Listener listener) {
            this.contact = contact;
            this.listener = new WeakReference<>(listener);
            service = new NewContactService();
        }

        @Override
        protected EditContactError doInBackground(Void... voids) {
            if (ContactValidator.isNewContactValid(contact)) {
                return service.run(contact);
            }
            return null;
        }

        @Override
        protected void onPostExecute(EditContactError error) {
            if (listener.get() != null) {
                if (error == null) {
                    listener.get().onNewContactCreated();
                } else {
                    listener.get().onEditError(error);
                }
            }
        }
    }

    public interface Listener {
        void onNewContactCreated();
        void onEditError(@NonNull EditContactError error);
    }
}

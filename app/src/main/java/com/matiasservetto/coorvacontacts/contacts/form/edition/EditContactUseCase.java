package com.matiasservetto.coorvacontacts.contacts.form.edition;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.matiasservetto.coorvacontacts.models.Contact;

import java.lang.ref.WeakReference;

public class EditContactUseCase {

    private BackgroundJob job;

    private EditContactUseCase() {

    }

    @NonNull
    public static EditContactUseCase run(@NonNull Contact contact, @NonNull Listener listener) {
        EditContactUseCase instance = new EditContactUseCase();
        instance.job = new BackgroundJob(contact, listener);
        instance.job.execute();
        return instance;
    }

    private static class BackgroundJob extends AsyncTask<Void, Void, EditContactError> {
        @NonNull
        private final Contact contact;
        @NonNull
        private final WeakReference<Listener> listener;
        @NonNull
        private final EditContactService service;

        private BackgroundJob(@NonNull Contact contact, @NonNull Listener listener) {
            this.contact = contact;
            this.listener = new WeakReference<>(listener);
            service = new EditContactService();
        }

        @Override
        protected EditContactError doInBackground(Void... voids) {
            return service.run(contact);
        }

        @Override
        protected void onPostExecute(EditContactError error) {
            if (listener.get() != null) {
                if (error == null) {
                    listener.get().onContactEdited();
                } else {
                    listener.get().onEditError(error);
                }
            }
        }
    }

    public interface Listener {
        void onContactEdited();

        void onEditError(@NonNull EditContactError error);
    }
}

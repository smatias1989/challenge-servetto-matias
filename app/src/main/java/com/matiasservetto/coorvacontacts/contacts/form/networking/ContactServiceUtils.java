package com.matiasservetto.coorvacontacts.contacts.form.networking;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.matiasservetto.coorvacontacts.contacts.form.edition.EditContactError;

public abstract class ContactServiceUtils {

    @Nullable
    public static EditContactError parseResponse(@Nullable String rawResponse, @NonNull String defaultError) {
        ApiResponse response = null;
        String message = defaultError;
        try {
            response = new Gson().fromJson(rawResponse, ApiResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (response != null && response.status != null && response.status == 0) {
            return null;
        }
        if (response != null && response.status != null && response.status == 1
                && response.message != null && !response.message.isEmpty()) {
            message += ": " + response.message;
        }
        return new EditContactError(message);
    }


}

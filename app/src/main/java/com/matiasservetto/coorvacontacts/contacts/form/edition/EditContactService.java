package com.matiasservetto.coorvacontacts.contacts.form.edition;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.matiasservetto.coorvacontacts.contacts.form.networking.ContactServiceUtils;
import com.matiasservetto.coorvacontacts.models.Contact;
import com.matiasservetto.coorvacontacts.networking.BaseService;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class EditContactService extends BaseService<String> {

    public interface ApiService {
        @POST("contacts/update/")
        Call<String> editContact(@Query("id") String id, @Body Contact body); // FIXME Response object came in string. should fix how to avois this problem and use Call<ApiResponse>
    }

    @NonNull
    private final ApiService service;

    public EditContactService() {
        service = createService(ApiService.class);
    }

    @Nullable
    public EditContactError run(@NonNull Contact contact) {
        String rawResponse = null;
        try {
            rawResponse = service.editContact(contact.id, contact).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ContactServiceUtils.parseResponse(rawResponse,"Error editing user");
    }
}

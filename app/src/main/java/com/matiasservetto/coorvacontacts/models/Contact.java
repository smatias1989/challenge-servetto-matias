package com.matiasservetto.coorvacontacts.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.matiasservetto.coorvacontacts.networking.BitmapDeserializer;

public class Contact implements Comparable<Contact>, Parcelable {

    @Nullable
    public String id;
    @SerializedName("first_name")
    @NonNull
    public String firstName;
    @SerializedName("last_name")
    @NonNull
    public String lastName;
    @NonNull
    public String email;
    @NonNull
    public String phone;
    @Nullable
    public Bitmap photo;

    public Contact(@Nullable String id, @NonNull String firstName, @NonNull String lastName, @NonNull String email, @Nullable String phone, @Nullable Bitmap photo) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone != null ? phone : "";
        this.photo = photo;
    }

    protected Contact(Parcel in) {
        id = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phone = in.readString();
        photo = BitmapDeserializer.base64ToBitmap(in.readString());
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    @Override
    public int compareTo(Contact o) {
        int compare = firstName.compareToIgnoreCase(o.firstName);
        if (compare != 0) {
            return compare;
        }
        return lastName.compareToIgnoreCase(o.lastName);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(BitmapDeserializer.bitmapToBase64(photo));
    }

    public void update(@NonNull Contact other) {
        firstName = other.firstName;
        lastName = other.lastName;
        email = other.email;
        phone = other.phone;
        photo = other.photo;
    }
}

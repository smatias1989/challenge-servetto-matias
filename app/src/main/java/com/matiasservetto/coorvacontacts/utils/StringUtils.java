package com.matiasservetto.coorvacontacts.utils;

import android.support.annotation.Nullable;

public class StringUtils {

    private StringUtils() {

    }

    public static boolean isNotEmpty(@Nullable String string) {
        return string != null && !string.isEmpty();
    }
}

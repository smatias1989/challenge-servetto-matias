package com.matiasservetto.coorvacontacts.utils;

import android.support.annotation.NonNull;

import com.matiasservetto.coorvacontacts.models.Contact;

public class ContactValidator {

    private ContactValidator() {
    }

    /**
     *
     * @return true if first name or email is completed and phone or email is completed
     */
    public static boolean isNewContactValid(@NonNull Contact contact) {
        if (contact.firstName.isEmpty()) {
            return false;
        }
        if (contact.lastName.isEmpty()) {
            return false;
        }
        if (contact.email.isEmpty()) {
            return false;
        }
        return true;
    }

}

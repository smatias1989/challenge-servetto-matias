package com.matiasservetto.coorvacontacts.networking;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.matiasservetto.coorvacontacts.BuildConfig;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseService<T> {


    private static final long TIMEOUT = 20;
    private static final String BASE_URL = "https://coorva-coding-challenge.herokuapp.com/api/v2/";

    @NonNull
    protected <T> T createService(@NonNull Class<T> service) {
        return new Retrofit.Builder()
                .client(createClientBuilder().build())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(createGson()))
                .build()
                .create(service);
    }

    @NonNull
    public static Gson createGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Bitmap.class, new BitmapDeserializer());
        return builder.create();
    }


    @NonNull
    private OkHttpClient.Builder createClientBuilder() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder builder = chain.request().newBuilder();
                        Map<String, String> headers = getHeaders();
                        for (String key : headers.keySet()) {
                            builder.addHeader(key, headers.get(key));
                        }
                        return chain.proceed(builder.build());
                    }
                });
        if (!BuildConfig.IS_RELEASE) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }
        return builder;
    }

    @NonNull
    public static Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type","application/json");
        return headers;
    }


}
